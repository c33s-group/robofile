# Base robo tasks to ease CI

## Installation

### Installation of the .robo Vendor
#### Quick

Go get the [Default RoboFile](https://gitlab.com/c33s-toolkit/robofile/raw/master/doc/default-RoboFile.php) for a quick start,

#### Manual

**or** put the following code on top of your `RoboFile.php`:

```php
<?php

echo RoboFile::ROBO_DIR;
(is_dir(RoboFile::ROBO_DIR) || mkdir(RoboFile::ROBO_DIR)) && chdir(RoboFile::ROBO_DIR);
if (!is_file('composer.json')) {
    exec('composer init --no-interaction', $output, $resultCode);
    exec('composer require c33s/robofile --no-interaction', $output, $resultCode);
    exec('rm composer.yaml || rm composer.yml || return true', $output, $resultCode2);
    if ($resultCode > 0) {
        copy('https://getcomposer.org/composer.phar', 'composer');
        exec('php composer require c33s/robofile --no-interaction');
        unlink('composer');
    }
} else {
    exec('composer install --dry-run --no-interaction 2>&1', $output);
    if (false === in_array('Nothing to install or update', $output)) {
        fwrite(STDERR, "\n##### Updating .robo dependencies #####\n\n")
        && exec('composer install --no-interaction');
    }
}
chdir('..');
require RoboFile::ROBO_DIR.'/vendor/autoload.php';
```

This will install `c33s/robofile` in .robo/vendor using composer (loaded temporarily if needed) and autoload it.

### Projects `.gitignore`
ci requires that you add the following folders to your projects `.gitignore` file.

#### Quick
simply run `robo ci:gitignore`

#### Manual
or add the folloing directories manually to your gitignore
```
/.robo/bin
/.robo/cache
/.robo/vendor
```

## Usage

You can use all the available tasks by including the main trait:

```php
class RoboFile extends \Robo\Tasks
{
    use \C33s\Robo\C33sTasks;
```

Or you may select them by hand to prevent collisions with your own tasks. Take a look at 
[`C33s\Robo\C33sTasks.php`](https://gitlab.com/c33s-toolkit/robofile/blob/master/src/C33sTasks.php)
to find out about the single modules. Please be aware that some of those tasks depend on others.

### Robo hooks debugging

A helper trait to list the order and arguments of all hooks triggered during
your robo command is also available. Just add the trait to your `RoboFile.php`:

```php
class RoboFile extends \Robo\Tasks
{
    use \C33s\Robo\DebugHooksTrait;
```

## CI modules, versions and hashes

The available CI modules can be found at [https://robo.c33s.services/](https://robo.c33s.services/). The project
sources are located at [https://gitlab.com/vworldat/ci-provider](https://gitlab.com/vworldat/ci-provider).

## Update dependencies

`c33s-toolkit/robo-file` can be updated by executing `robo ci:self-update`. If this is somehow broken,
just change into the `.robo` directory and run `composer update`.
