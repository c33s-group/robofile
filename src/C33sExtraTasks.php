<?php

namespace C33s\Robo;

use C33s\Robo\Task\Extra\AssetsTasks;
use C33s\Robo\Task\Extra\CheckTasks;
use C33s\Robo\Task\Extra\CodeceptionTasks;
use C33s\Robo\Task\Extra\DeployTasks;
use C33s\Robo\Task\Extra\ErrorHandling;
use C33s\Robo\Task\Extra\FileTasks;
use C33s\Robo\Task\Extra\QuietExecTasks;
use C33s\Robo\Task\Extra\VersionTasks;

trait C33sExtraTasks
{
    use DeployTasks;
    use CodeceptionTasks;
    use AssetsTasks;
    use FileTasks;
    use VersionTasks;
    use QuietExecTasks;
    use ErrorHandling;
    use CheckTasks;
}
