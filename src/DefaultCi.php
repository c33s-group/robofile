<?php

namespace C33s\Robo;

interface DefaultCi
{
    /**
     * Initialize project.
     */
    public function init();

    /**
     * Perform code-style checks.
     */
    public function check();

    /**
     * Perform code-style checks and cleanup source code automatically.
     */
    public function fix();

    /**
     * Run tests.
     */
    public function test();
}
