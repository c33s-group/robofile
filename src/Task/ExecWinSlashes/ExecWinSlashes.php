<?php

namespace C33s\Robo\Task\ExecWinSlashes;

use Robo\Task\Base\Exec;

class ExecWinSlashes extends Exec
{
    private $fixWindowsSlashes = true;

    /**
     * Set to true to automatically replace slashes in the command arguments with backslashes on windows.
     *
     * @param bool $fixWindowsSlashes
     *
     * @return $this
     */
    public function fixWindowsSlashes($fixWindowsSlashes = true)
    {
        $this->fixWindowsSlashes = (bool) $fixWindowsSlashes;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getCommand()
    {
        $command = parent::getCommand();
        if ($this->fixWindowsSlashes) {
            $command = static::fixCommandSlashes($command);
        }

        return $command;
    }

    /**
     * Smart Slashes Fixer based upon OS.
     *
     * @param string $command
     *
     * @return string
     */
    public static function fixCommandSlashes($command)
    {
        if ('.' == $command[0] || '/' == $command[0]) {
            if (!stristr($command, ' ')) {
                return str_replace('/', DIRECTORY_SEPARATOR, $command);
            }
            $commandParts = explode(' ', $command, 2);

            $firstCommandPart = str_replace('/', DIRECTORY_SEPARATOR, $commandParts[0]);
            $secondCommand = $commandParts[1];

            return $firstCommandPart.' '.$secondCommand;
        }

        return $command;
    }
}
