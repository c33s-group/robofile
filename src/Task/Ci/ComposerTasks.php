<?php

namespace C33s\Robo\Task\Ci;

use Exception;

trait ComposerTasks
{
    protected function composerGlobalRequire($module, $version)
    {
        try {
            $this->_execPhp("./{$this->dir()}/bin/composer.phar global why {$module} --quiet");
        } catch (Exception $e) {
            $this->_execPhp("./{$this->dir()}/bin/composer.phar global require \"{$module}:{$version}\" --no-progress");
        }
    }
}
