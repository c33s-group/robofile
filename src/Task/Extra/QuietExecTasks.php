<?php

namespace C33s\Robo\Task\Extra;

use Robo\Exception\TaskExitException;

trait QuietExecTasks
{
    /**
     * Executes shell command and allows to add variables to the environment
     * Defined in trait ` LoadImprovedExecShortcut`.
     *
     * @param string|\Robo\Contract\CommandInterface $command
     * @param array                                  $mergeEnvironment
     * @param null                                   $environment
     * @param bool|null                              $interactive
     *
     * @return \Robo\Result
     */
    abstract protected function _exec($command, $mergeEnvironment = [], $environment = null, $interactive = false);

    /**
     * On `execs` robo shows the text of the exception directly from the command executed and after the command was run
     * it also shows the output text which also contains the exception text. to prevent this use this exec.
     *
     * @param $command
     */
    protected function _execQuiet($command)
    {
        try {
            $this->_exec($command);
        } catch (TaskExitException $exception) {
            $this->throwReducedErrorException();
        }
    }
}
