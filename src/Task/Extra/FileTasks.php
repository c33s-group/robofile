<?php

namespace C33s\Robo\Task\Extra;

trait FileTasks
{
    /**
     * https://tomnomnom.com/posts/realish-paths-without-realpath.
     *
     * @param $filename
     *
     * @return string
     */
    protected function resolveFilename($filename)
    {
        $filename = str_replace('//', '/', $filename);
        $parts = explode('/', $filename);
        $out = [];
        foreach ($parts as $part) {
            if ('.' == $part) {
                continue;
            }
            if ('..' == $part) {
                array_pop($out);
                continue;
            }
            $out[] = $part;
        }

        return implode('/', $out);
    }

    protected function fileContains($file, $needle)
    {
        if (false !== strpos(file_get_contents($file), $needle)) {
            return true;
        }

        return false;
    }

    protected function makeWindowsPath($path)
    {
        return str_replace('/', '\\', $path);
    }

    protected function makeLinuxPath($path)
    {
        return str_replace('\\', '/', $path);
    }

    protected function failOnNotExist($file)
    {
        if (file_exists($file)) {
            return;
        }

        throw new Exception("file or directory '$file' does not exist.");
    }

    protected function isJunction($path)
    {
        if (!file_exists($path)) {
            return false;
        }
        $stat = stat($path);
        $lstat = lstat($path);
        if (!empty((array_diff($stat, $lstat)))) {
            return true;
        }

        return false;
    }

    protected function removeJunction($path)
    {
        rmdir($path);
    }

    protected function junctionWithSymlinkFallback($from, $to)
    {
        // only handle directories on windows which target is a directory
        if (!$this->isWin() || !is_dir($to)) {
            return $this->_symlink($from, $to);
        }

        if ($this->isJunction($from)) {
            //junction link already there
            return;
        }

        $fromWindows = $this->makeWindowsPath($from);
        $toWindows = $this->makeWindowsPath($to);

        return $this->_exec("mklink /D /J $fromWindows $toWindows");
    }

    /**
     * Windows safe directory delete, because recursive unlink on windows often fails for .git files.
     *
     * @param array $dirs
     */
    protected function deleteDirWindowsSafe($dirs)
    {
        foreach ($dirs as $dir) {
            if (!file_exists($dir)) {
                continue;
            }
            if ($this->isWin()) {
                $winDir = $this->makeWindowsPath($dir);
                $this->_exec("rd /S /Q $winDir");
                continue;
            }
            $this->taskFilesystemStack()->remove($dir);
        }
    }
}
