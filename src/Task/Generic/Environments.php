<?php

namespace C33s\Robo\Task\Generic;

use Exception;
use Robo\Robo;
use Symfony\Component\Console\Input\InputOption;

trait Environments
{
    /**
     * As trait properties cannot be overridden, to override this property you have to add a constructor in your robofile
     * and set the value there.
     *
     * @var string
     */
    protected $environmentsConfigKey = 'sf-env';

    /**
     * As trait properties cannot be overridden, to override this property you have to add a constructor in your robofile
     * and set the value there.
     *
     * @var string
     */
    protected $environmentsConfigKeyShort = null;

    /**
     * @hook pre-option
     */
    public function addEnvironmentOptionToEveryCommand()
    {
        $inputOption = new InputOption(
            $this->environmentsConfigKey,
            $this->environmentsConfigKeyShort,
            InputOption::VALUE_OPTIONAL,
            'Provide custom environment name that may override environment variables'
        );

        foreach (Robo::application()->all() as $command) {
            $command->getDefinition()->addOption(clone $inputOption);
        }
    }

    /**
     * Check if all the given environment variables have been set and abort if not.
     *
     * @param string[] $names
     */
    protected function checkSymfonyEnvOrAbort($names) //TODO: php7 - array
    {
        foreach ($names as $name) {
            if (!$this->hasSymfonyEnv($name)) {
                $this->abort("Environment variable '$name' is required.");
            }
        }
    }

    /**
     * Check if the given environment variable has been defined.
     *
     * @param string $name
     *
     * @return bool
     */
    protected function hasSymfonyEnv($name) //TODO: php7 - string //TODO: php7 -  bool
    {
        // phpcs:disable PHPCompatibility.FunctionUse.RequiredToOptionalFunctionParameters.getenv_varnameMissing
        if (PHP_VERSION_ID < 70100) {
            throw new Exception('environment function is not supported on your php version');
        }
        if (array_key_exists($name, $_ENV)) {
            return true;
        }
        if (false !== getenv($name, true)) {
            return true;
        }
        if (array_key_exists($name, getenv())) {
            return true;
        }
        // phpcs:enable PHPCompatibility.FunctionUse.RequiredToOptionalFunctionParameters.getenv_varnameMissing

        return false;
    }

    /**
     * Get environment variable value or the given default value if not set.
     *
     * @param string $name
     * @param mixed  $default
     *
     * @return mixed|null
     */
    protected function getSymfonyEnv($name, $default = null) //TODO: php7 - string
    {
        // phpcs:disable PHPCompatibility.FunctionUse.RequiredToOptionalFunctionParameters.getenv_varnameMissing
        if (PHP_VERSION_ID < 70100) {
            throw new Exception('environment function is not supported on your php version');
        }
        if (array_key_exists($name, $_ENV)) {
            return $_ENV[$name];
        }
        if (false !== getenv($name, true)) {
            return getenv($name, true);
        }
        if (array_key_exists($name, getenv())) {
            return getenv()[$name];
        }
        // phpcs:enable PHPCompatibility.FunctionUse.RequiredToOptionalFunctionParameters.getenv_varnameMissing

        return $default;
    }
}
