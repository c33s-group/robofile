<?php

namespace C33s\Robo\Task\Generic;

use Consolidation\AnnotatedCommand\CommandData;
use Psr\Log\LoggerInterface;
use Robo\Robo;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Dotenv\Dotenv;
use Symfony\Component\Yaml\Yaml;

/**
 * @deprecated
 *
 * @method LoggerInterface getLogger()
 */
trait DeploymentEnvironments
{
    /**
     * As trait properties cannot be overridden, to override this property you have to add a constructor in your robofile
     * and set the value there.
     *
     * @var string
     *
     * @deprecated
     */
    protected $deploymentEnvironmentsFile = 'environments.yaml';

    /**
     * As trait properties cannot be overridden, to override this property you have to add a constructor in your robofile
     * and set the value there.
     *
     * @var string
     *
     * @deprecated
     */
    protected $deploymentEnvironmentConfigKey = 'env';

    /**
     * As trait properties cannot be overridden, to override this property you have to add a constructor in your robofile
     * and set the value there.
     *
     * @var string
     *
     * @deprecated
     */
    protected $deploymentEnvironmentConfigKeyShort = 'e';

    /**
     * @var array
     */
    protected $deploymentEnvironmentConfigs = [];

    /**
     * @hook pre-option
     *
     * @deprecated
     */
    public function addDeploymentEnvironmentOptionToEveryCommand()
    {
        $inputOption = new InputOption(
            $this->deploymentEnvironmentConfigKey,
            $this->deploymentEnvironmentConfigKeyShort,
            InputOption::VALUE_OPTIONAL,
            'Provide custom environment name that may override environment variables'
        );

        foreach (Robo::application()->all() as $command) {
            $command->getDefinition()->addOption(clone $inputOption);
        }
    }

    /**
     * This should be the first element in your RoboFile's @pre-command hook.
     *
     * @param CommandData $commandData
     *
     * @deprecated
     */
    protected function assignEnvironmentConfig($commandData) //TODO: php7 - CommandData
    {
        $logger = $this->getLogger();
        $logger->info('Loading environment config');
        if (!$commandData->input()->hasOption($this->deploymentEnvironmentConfigKey)) {
            $logger->debug('Environment config option is not available');

            return;
        }
        $envName = $commandData->input()->getOption($this->deploymentEnvironmentConfigKey);
        if (null === $envName) {
            $logger->debug('No custom environment was set');

            return;
        }

        $logger->info("Custom environment is $envName. Loading config from files.");
        $config = $this->loadEnvironmentConfigFromFiles();
        if (!isset($config[$envName]) || !is_array($config[$envName])) {
            $this->abort("Cannot find environment specific config for: $envName");
        }
        $dotenv = new Dotenv(true);
        $dotenv->populate($config[$envName]);
        $logger->debug(
            "Assigned environment config for $envName containing keys: ".implode(', ', array_keys($config[$envName]))
        );
    }

    /**
     * Load all available extra environment config values into a single array.
     *
     * @return array
     *
     * @deprecated
     */
    protected function loadEnvironmentConfigFromFiles() //TODO: php7 -  array
    {
        $file = $this->deploymentEnvironmentsFile;
        $config = $this->loadSingleEnvironmentConfigFile($file.'.dist');
        $moreConfig = $this->loadSingleEnvironmentConfigFile($file);
        $this->getLogger()->debug('Merging config values');
        foreach ($moreConfig as $env => $vars) {
            if (!isset($config[$env])) {
                $config[$env] = [];
            }
            foreach ($vars as $name => $var) {
                $config[$env][$name] = $var;
            }
        }

        return $config;
    }

    /**
     * @param string $filename
     *
     * @return array
     *
     * @deprecated
     */
    protected function loadSingleEnvironmentConfigFile($filename) //TODO: php7 - string //TODO: php7 -  array
    {
        $logger = $this->getLogger();
        $logger->debug("Loading environment config from $filename");
        $config = [];
        if (!is_file($filename)) {
            $logger->debug("Environment file $filename not found.");

            return $config;
        }

        $content = Yaml::parse(file_get_contents($filename));
        if (null === $content) {
            $logger->debug("$filename is empty");

            return $config;
        }
        if (!is_array($content)) {
            throw new \LogicException("Environment config file $filename does not contain array data");
        }

        foreach ($content as $envName => $variables) {
            if (null === $variables) {
                $config[$envName] = [];
            } elseif (is_array($variables)) {
                foreach ($variables as $key => $value) {
                    $config[$envName][(string) $key] = (string) $value;
                }
            } else {
                throw new \LogicException("$filename does not contain array data for environment $envName");
            }
        }
        $logger->debug("$filename contained config for ".implode(', ', array_keys($config)));

        return $config;
    }

    /**
     * Check if all the given environment variables have been set and abort if not.
     *
     * @param string[] $names
     *
     * @deprecated
     */
    protected function checkEnv($names)
    {
        $this->checkSymfonyEnvOrAbort($names);
    }

    /**
     * Check if the given environment variable has been defined.
     *
     * @param string $name
     *
     * @return bool
     *
     * @deprecated
     */
    protected function hasEnv($name)
    {
        return $this->hasSymfonyEnv($name);
    }

    /**
     * Get environment variable value or the given default value if not set.
     *
     * @param string $name
     * @param mixed  $default
     *
     * @return mixed|null
     *
     * @deprecated
     */
    protected function getEnv($name, $default = null)
    {
        return $this->getSymfonyEnv($name, $default);
    }
}
