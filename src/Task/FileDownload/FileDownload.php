<?php

namespace C33s\Robo\Task\FileDownload;

use Robo\Common\BuilderAwareTrait;
use Robo\Contract\BuilderAwareInterface;
use Robo\Exception\TaskException;
use Robo\Result;
use Robo\Task\BaseTask;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Process\ExecutableFinder;
use Webmozart\Assert\Assert;

class FileDownload extends BaseTask implements BuilderAwareInterface
{
    use BuilderAwareTrait;

    const METHOD_AUTO = 'auto';
    const METHOD_FOPEN = 'fopen';
    const METHOD_CURL = 'curl';
    const METHOD_WGET = 'wget';

    private $availableMethods = [
        self::METHOD_AUTO,
        self::METHOD_FOPEN,
        self::METHOD_CURL,
        self::METHOD_WGET,
    ];

    /**
     * @var string
     */
    private $remoteUri;

    /**
     * @var string
     */
    private $localPath;

    /**
     * @var string
     */
    private $method = self::METHOD_AUTO;

    /**
     * @var string
     */
    private $pathToWget;

    /**
     * Download the given file.
     *
     * @param string $remoteUri
     */
    public function __construct($remoteUri) // TODO: php7 string
    {
        $this->remoteUri = $remoteUri;
    }

    /**
     * The local file path to download to. If not specified, the filename will be determined
     * based on the download URI and stored to the current directory.
     *
     * @param string $localPath
     *
     * @return self
     */
    public function toFile($localPath = null) //TODO: php7 - string //TODO: php7 -  self
    {
        $this->localPath = $localPath;

        return $this;
    }

    public function usingMethod($method) //TODO: php7 - string //TODO: php7 -  self
    {
        if (!in_array($method, $this->availableMethods)) {
            $error = "Unknown download method '$method'. Available methods are ".implode(', ', $this->availableMethods);
            throw new TaskException(static::class, $error);
        }
        $this->method = $method;

        return $this;
    }

    public function usingFopen() //TODO: php7 -  self
    {
        return $this->usingMethod('fopen');
    }

    public function usingCurl() //TODO: php7 -  self
    {
        return $this->usingMethod('curl');
    }

    public function usingWget($pathToWget = null) //TODO: php7 - string //TODO: php7 -  self
    {
        $this->pathToWget = $pathToWget;

        return $this->usingMethod('wget');
    }

    /**
     * Auto-detect download method.
     *
     * @return string|null
     */
    private function autoDetectMethod() //TODO: php7 -  ? string
    {
        if ($this->isWgetExecutableAvailable()) {
            return self::METHOD_WGET;
        } elseif ($this->isCurlExtensionAvailable()) {
            return self::METHOD_CURL;
        } elseif ($this->isAllowUrlFopenEnabled()) {
            return self::METHOD_FOPEN;
        }

        return null;
    }

    private function createLocalDirectoryForFile()
    {
        $fs = new Filesystem();
        $dir = dirname($this->localPath);
        $this->printTaskDebug("Checking if download dir '$dir' exists.");
        if ($fs->exists($dir)) {
            $this->printTaskDebug('Download dir already exists.');

            return;
        }

        $this->printTaskInfo('Creating download dir.');
        $fs->mkdir($dir);
    }

    private function isAllowUrlFopenEnabled() //TODO: php7 -  bool
    {
        return ini_get('allow_url_fopen');
    }

    private function downloadFileUsingFopen()
    {
        copy($this->remoteUri, $this->localPath);
    }

    private function isCurlExtensionAvailable() //TODO: php7 -  bool
    {
        return extension_loaded('curl');
    }

    private function downloadFileUsingCurl()
    {
        $options = [
            CURLOPT_FILE => fopen($this->localPath, 'w'),
            CURLOPT_TIMEOUT => 300,
            CURLOPT_URL => $this->remoteUri,
            CURLOPT_FOLLOWLOCATION => 1,
        ];

        $curlHandler = curl_init();
        curl_setopt_array($curlHandler, $options);
        curl_exec($curlHandler);
        curl_close($curlHandler);
    }

    private function isWgetExecutableAvailable() //TODO: php7 -  bool
    {
        return null !== $this->detectWgetExecutable();
    }

    private function detectWgetExecutable() //TODO: php7 -  ? string
    {
        $finder = new ExecutableFinder();

        return $finder->find('wget');
    }

    private function downloadFileUsingWget()
    {
        $wget = $this->detectWgetExecutable();
        $command = "$wget {$this->remoteUri} --output-document={$this->localPath} --quiet --";
        $this->collectionBuilder()->taskExec($command)->run();
    }

    private function checkMethod() //TODO: php7 -  string
    {
        $method = getenv('PHP_ROBO_DEFAULT_DOWNLOAD_METHOD');
        if (self::METHOD_AUTO === $this->method && in_array($method, $this->availableMethods)) {
            $this->printTaskDebug("Using '$method' as default download method.");
            Assert::string($method);
            $this->method = $method;
        }

        $this->printTaskDebug("Checking download method. Configured method is '$this->method'");
        $errorHint = null;
        switch ($this->method) {
            case self::METHOD_FOPEN:
                if (!$this->isAllowUrlFopenEnabled()) {
                    $errorHint = 'enable allow_url_fopen';
                }
                break;

            case self::METHOD_CURL:
                if (!$this->isCurlExtensionAvailable()) {
                    $errorHint = 'install and enable CURL extension';
                }
                break;

            case self::METHOD_WGET:
                if (null !== $this->pathToWget && !is_executable($this->pathToWget)) {
                    $errorHint = 'provide a valid wget executable path';
                } elseif (!$this->isWgetExecutableAvailable()) {
                    $errorHint = 'put wget executable into your PATH or provide custom path in ->usingWget($customPath)';
                }
                break;

            default:
                $this->method = $this->autoDetectMethod();
                if (null === $this->method) {
                    throw new TaskException(
                        static::class,
                        <<<ERROR
Could not detect any download method. Please either:
  - enable allow_url_fopen
  - install and enable CURL extension
  - or provide a valid wget executable
ERROR
                    );
                }
                $this->printTaskDebug("Auto-detected download method. Using '$this->method'");
        }

        if (null !== $errorHint) {
            throw new TaskException(
                static::class,
                "Please $errorHint or choose another download method"
            );
        }

        return $this->method;
    }

    private function doDownload($method) //TODO: php7 - string
    {
        $this->printTaskInfo("Using $method to download $this->remoteUri to $this->localPath");
        switch ($method) {
            case self::METHOD_FOPEN:
                $this->downloadFileUsingFopen();
                break;

            case self::METHOD_CURL:
                $this->downloadFileUsingCurl();
                break;

            case self::METHOD_WGET:
                $this->downloadFileUsingWget();
                break;

            default:
                throw new TaskException(
                    static::class,
                    "Cannot download file. Unknown download method '$method'"
                );
        }
    }

    private function checkLocalPath()
    {
        if (null === $this->localPath) {
            $this->localPath = basename($this->remoteUri);
            $this->printTaskInfo("Local path was auto-detected: '$this->localPath'");
        }
    }

    /**
     * @return Result
     *
     * @throws TaskException
     */
    public function run()
    {
        $method = $this->checkMethod();
        $this->checkLocalPath();
        $this->createLocalDirectoryForFile();
        $this->doDownload($method);

        return new Result($this, 0, "Successfully downloaded $this->remoteUri to $this->localPath");
    }
}
