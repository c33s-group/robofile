<?php

namespace C33s\Robo\Task\FileDownload;

use Robo\Exception\TaskException;
use Robo\Result;

trait loadTasks
{
    /**
     * Download the given file and store it locally.
     *
     * @param string $remoteUri
     *
     * @return FileDownload
     */
    protected function taskFileDownload($remoteUri) //TODO: php7 - string
    {
        return $this->task(FileDownload::class, $remoteUri);
    }

    /**
     * @return Result
     *
     * @throws TaskException
     */
    protected function _downloadFileToLocalPath(
        $remoteUri,
        $localPath,
        $method = FileDownload::METHOD_AUTO
    ) {
        return $this
            ->taskFileDownload($remoteUri)
            ->toFile($localPath)
            ->usingMethod($method)
            ->run()
        ;
    }
}
