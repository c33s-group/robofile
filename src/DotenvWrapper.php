<?php

namespace C33s\Robo;

use Symfony\Component\Dotenv\Dotenv;

/**
 * Class DotenvWrapper.
 *
 * @deprecated
 */
class DotenvWrapper
{
    /**
     * Load the given files using Symfony's Dotenv component, but only if the file exists.
     * Be aware that existing variables will not be overridden by later files!
     *
     * @deprecated
     */
    public static function loadIfExists(...$paths)
    {
        if (!defined('C33S_SKIP_LOAD_DOT_ENV')) {
            $dotEnv = new Dotenv(true);
            foreach ($paths as $path) {
                if (file_exists($path)) {
                    $dotEnv->load($path);
                }
            }
        }
    }
}
